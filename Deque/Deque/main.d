module main;

import std.stdio;
import std.array, std.string;
struct Deque(T)
{
	public:
	void push_front(T x)
	{
		_front.length += 1;
		_front[$-1] = x;
	}
	void push_back(T x)
	{
		_back.length += 1;
		_back[$-1] = x;
	}
	//pop one off of the back of front
	T pop_front() 
	in
	{assert(this.size() > 0);}
	body
	{
		if(this.size == 1 && _back.length == 1)
		{
			return pop_back();
		}
		else if(_front.length == 0)
		{
			balance();
			T temp = _front[_front.length-1];
			_front.length -= 1;
			return temp;
		}
		else
		{
			T temp = _front[_front.length-1];
			_front.length -= 1;
			return temp;
		}
	}
	
	T pop_back() 
	in
	{assert(this.size() > 0);}
	body
	{
		if(this.size == 1 && _front.length == 1)
		{
			return	pop_front();
		}
		else if(_back.length == 0)
		{
			balance();
			T temp = _back[_back.length-1];
			_back.length -= 1;
			return temp;
		}
		else
		{
			T temp = _back[_back.length-1];
			_back.length -= 1;
			return temp;
		}
	}

	ref T at(uint pos) 
	in {assert (pos >= 0 && pos < this.size());}
	body
	{
		if(_front.length > pos)
			return _front[_front.length - 1 - pos];
		else
			return _back[pos - _front.length];
	}

	ref T back()
	in{assert (this.size() > 0);}
	body
	{
		if(_back.length == 0)
			return _front[0];
		else
			return _back[$-1];
	}

	ref T front() 
		in {assert (this.size() >= 0);}
	body
	{
		if(_front.length == 0)
			return _back[0];
		else
			return _front[$-1];
	}

	uint size()
	{
		return _front.length + _back.length;
	}
	private:
	void dump()
	{
		if(this.size > 0)
		{
			foreach_reverse(x; _front)
				writeln(x);
			foreach(x; _back)
				writeln(x);
		}
	}
	void balance()
	{
		writeln("balancing...");
		if(_front.length >= 2)
		{
			int mid = (this.size()/2);
			_back = _front[mid..$];
			_front = _front[0..mid];
		}
		else
		{
			int mid = (this.size() / 2);
			_front = _back[0..mid];
			_back = _back[mid..$];
		}
	}

	T[] _front;
	T[] _back;
}

		unittest {
			Deque!(int) d;
			d.push_front(1);
			d.push_back(2);
			assert(d.at(0) == 1);
			assert(d.at(1) == 2);
			assert(d.size() == 2);
			d.push_front(3);
			d.push_back(4);
			assert(d.back() == 4);
			assert(d.front() == 3);
			d.at(1) = 10;
			d.front() = 30;
			d.back() = 40;
			assert(d.back() == 40);
			assert(d.front() == 30);
			assert(d.at(1) == 10);

			d.dump();
			writeln(d.pop_front());
			writeln(d.pop_front());
			writeln(d.pop_front());
			writeln(d.pop_front());
			d.dump();

			Deque!(string) d2;
			d2.push_front("one");
			d2.push_back("two");
			assert(d2.at(0) == "one");
			assert(d2.at(1) == "two");
			assert(d2.size() == 2);
			d2.push_front("three");
			d2.push_back("four");
			assert(d2.back() == "four");
			assert(d2.front() == "three");
			d2.at(1) = "ten";
			d2.front() = "thirty";
			d2.back() = "forty";
			assert(d2.back() == "forty");
			assert(d2.front() == "thirty");
			assert(d2.at(1) == "ten");

			d2.dump();
			writeln(d2.pop_front());
			writeln(d2.pop_front());
			writeln(d2.pop_front());
			writeln(d2.pop_front());
			d2.dump();

			writeln("unit test passed");
		}



void main(string[] argv)
{
}
