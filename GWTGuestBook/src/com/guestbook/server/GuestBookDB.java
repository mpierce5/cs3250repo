package com.guestbook.server;

import java.text.DateFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Vector;
public class GuestBookDB {
    public GuestBookEntry[] getDB() {
        return (GuestBookEntry[])DB.toArray(new GuestBookEntry[0]);
    }
    public void addEntry(String name, String comment) {
        DB.addElement(new GuestBookEntry(name, comment, getDateTime()));
    }
    public String getDateTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    Vector DB = new Vector(10);
}


