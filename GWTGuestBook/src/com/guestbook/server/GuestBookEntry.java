package com.guestbook.server;

//stores name, entry, and when it was entered
public class GuestBookEntry {


    public GuestBookEntry(String name, String comment, String date) {
        this.name = name;
        this.comment = comment;
        this.date = date;
    }
    public String getName() {
        return name;
    }
    public String getComment() {
        return comment;
    }
    public String getDate() {
        return date;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public void setDate(String date) {
        this.date = date;
    }
String name;
String comment;
String date;
}