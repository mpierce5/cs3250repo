package com.guestbook.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.guestbook.client.guestbookService;

public class guestbookServiceImpl extends RemoteServiceServlet implements guestbookService {
    public String addNameAndEntry(String name, String entry){
        if (!name.equals(""))
            GBDatabase.addEntry(name, entry);
        String retval = "<p>";
        if (GBDatabase.getDB().length > 0)
            for (GuestBookEntry x : GBDatabase.getDB())
                retval += x.getName() + ": " + x.getComment() + " " + x.getDate() + "<br />";
        return retval + "</p>";
    }
    GuestBookDB GBDatabase = new GuestBookDB();
}