package com.guestbook.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("guestbookService")
public interface guestbookService extends RemoteService {
    String addNameAndEntry(String name, String entry);
    public static class App {
        private static guestbookServiceAsync ourInstance = GWT.create(guestbookService.class);
        public static synchronized guestbookServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
