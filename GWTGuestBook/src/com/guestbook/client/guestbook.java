package com.guestbook.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.DOM;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;

public class guestbook implements EntryPoint {

    public void onModuleLoad() {
        final TextBox nameTB = new TextBox();
        final TextBox entryTB = new TextBox();
        final Button button = new Button("Add Entry");
        final Label userName = new Label();
        final Label comment = new Label();
        final HTML allEntries = new HTML("<p></p>");
        userName.setText("User Name: ");
        comment.setText("comment: ");
        button.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (!nameTB.getText().equals(""))
                {
                    guestbookService.App.getInstance().addNameAndEntry(nameTB.getText(), entryTB.getText(), new MyAsyncCallback(allEntries));
                }
            }
        });
        RootPanel.get("buttonSpot").add(button);
        RootPanel.get("user name label").add(userName);
        RootPanel.get("user name TB").add(nameTB);
        RootPanel.get("comment label").add(comment);
        RootPanel.get("comment TB").add(entryTB);
        RootPanel.get("allEntries").add(allEntries);
        guestbookService.App.getInstance().addNameAndEntry("", "", new MyAsyncCallback(allEntries));
    }
    private static class MyAsyncCallback implements AsyncCallback<String> {
        private HTML html;
        public MyAsyncCallback(HTML html) {
            this.html = html;
        }
        public void onSuccess(String result) {
            html.setHTML(result);
        }
        public void onFailure(Throwable throwable) {
            html.setHTML("<p>Failed to receive answer from server! " + throwable.toString() + "</p>");
        }
    }
}