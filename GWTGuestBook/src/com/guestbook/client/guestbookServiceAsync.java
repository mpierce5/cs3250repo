package com.guestbook.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface guestbookServiceAsync {
    void addNameAndEntry(String name, String entry, AsyncCallback<String> async);
}
