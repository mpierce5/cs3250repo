set PATH=C:\D\dmd2\windows\bin;C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\\bin;%PATH%
dmd -g -debug -X -Xf"Debug\Gettysburg.json" -deps="Debug\Gettysburg.dep" -of"Debug\Gettysburg.exe_cv" -map "Debug\Gettysburg.map" -L/NOMAP main.d
if errorlevel 1 goto reportError
if not exist "Debug\Gettysburg.exe_cv" (echo "Debug\Gettysburg.exe_cv" not created! && goto reportError)
echo Converting debug information...
"C:\Program Files (x86)\VisualD\cv2pdb\cv2pdb.exe" -D2 "Debug\Gettysburg.exe_cv" "Debug\Gettysburg.exe"
if errorlevel 1 goto reportError
if not exist "Debug\Gettysburg.exe" (echo "Debug\Gettysburg.exe" not created! && goto reportError)

goto noError

:reportError
echo Building Debug\Gettysburg.exe failed!

:noError
