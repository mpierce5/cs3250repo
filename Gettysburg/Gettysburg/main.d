module main;

import std.stdio;
import std.array;
import std.string;
import std.file;

void main(string[] argv)
{
	int[int][string] myArray;
	foreach(int lineNumber, string theLine; lines(stdin))
	{
		foreach(temp; split(theLine))
		{
			myArray[temp][lineNumber+1]++;
		}
	}
	foreach(key; myArray.keys.sort)
	{
		write(key ~ ": ");
		foreach(num; myArray[key].keys.sort)
		{
			write(num, ":", myArray[key][num], " ,");
		}
		writeln();
	}
}
