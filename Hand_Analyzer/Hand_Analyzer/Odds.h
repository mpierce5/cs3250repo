#pragma once
#include <string>
#include "Deck.h"
#include <iostream>

using namespace std;
class Odds
{
public:
	Odds(void);
	~Odds(void);
	double calcPotOdds(double potSize, double betSize);
	double oddsMakeHand(Deck myDeck);
private:
	int checkForHighPair(Deck myDeck);
	int checkFor2Pair(Deck myDeck);
	int checkForStraight(Deck myDeck);
	int checkFor3ofKind(Deck myDeck);
	int checkForFlush(Deck myDeck);
	int checkForFullHouse(Deck myDeck);
	int checkFor4ofKind(Deck myDeck);
	int checkForStraightFlush(Deck myDeck);
	int getTableHand(Deck myDeck);
	
	double currentOdds;
	int cardsInPlay[7];
};

