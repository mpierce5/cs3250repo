#include "Odds.h"


Odds::Odds()
{
	Deck deck = Deck();
	for(int i = 0; i < 7; i++)
	cardsInPlay[i] = -1;
}


Odds::~Odds(void)
{
}
//calculate the odds of making the best possible hand with one card
double Odds::oddsMakeHand(Deck myDeck)
{
	// 0 - 12 clubs
	// 14 - 25 diamonds
	// 28 - 38 hearts
	// 39 - 51 spades
	//calculate outs
	int j = 0;
	for(int i = 0; i < 52; i++)
	{
		if(myDeck.deck[i] == true)
		{
			cardsInPlay[j] = i;
			j++;
		}
	}

	for(int i = 0; i < 7; i++)
	{
		if(cardsInPlay[i] >= 0)
		cout << cardsInPlay[i] << " is in play\n";
	}

	//checkForHighPair(myDeck);
	checkFor2Pair(myDeck);
	return 0;
	//calculate hand desired
	//first check for high pair


	//calculate odds
}

int Odds::checkForHighPair(Deck myDeck)
{
	//returns -1 if no match if found, or returns value between 0-12, 0 being highest priority
	//12 being lowest priority
	int priority = -1;
	//check for a pocket pair
	int pCard1 = myDeck.pocketCards[0];
	int pCard2 = myDeck.pocketCards[1];
	int a = pCard1 % 13;
	int b = pCard2 % 13;
	if(a == b)
	{
		if(priority < a) priority = a;
		cout << "holding pocket pair with " << a << "\n";
	}
	//to check for a match, compare pocket cards to cards on table
	for(int j = 0; j < 52; j++)
	{
		//check for pairs made by pocket cards, not pairs already on board
		if(pCard1 != j && pCard2 != j && myDeck.deck[j] == true && a == j%13)
		{
			if(priority == -1) priority = a;
			else if(priority > a) priority = a;
			cout << "Pair made " << pCard1 << " with " << j << "priority " << a << "\n";
		}
		if(pCard2 != j && pCard1 != j && myDeck.deck[j] == true && b == j%13)
		{
			if(priority == -1) priority = b;
			else if(priority > a) priority = b;
			cout << "Pair made " << pCard2 << " with " << j << "priority " << b << "\n";
		}
	}
	return priority;
}

int Odds::checkFor2Pair(Deck myDeck)
{
	int priority = -1;
	//check for a pocket pair
	int pCard1 = myDeck.pocketCards[0];
	int pCard2 = myDeck.pocketCards[1];
	bool match1 = false;
	bool match2 = false;

	int a = pCard1 % 13;
	int b = pCard2 % 13;
	if(a == b)
	{
		if(priority < a) priority = a;
		match1 = true;
		cout << "holding pocket pair with " << a << "\n";
	}
	//to check for a match, compare pocket cards to cards on table
	for(int j = 0; j < 52; j++)
	{
		//check for pairs made by pocket cards, not pairs already on board
		if(pCard1 != j && pCard2 != j && myDeck.deck[j] == true && a == j%13)
		{
			if(priority == -1) priority = a;
			else if(priority > a) priority = a;
			if(match1 == true) 
			{
				match2 = true;
			}
			else 
				match1 = true;

			cout << "Pair made " << pCard1 << " with " << j << "priority " << a << "\n";
		}
		if(pCard2 != j && pCard1 != j && myDeck.deck[j] == true && b == j%13)
		{
			if(priority == -1) priority = b;
			else if(priority > a) priority = b;
			if(match1 == true) 
			{
				match2 = true;
			}
			else 
				match1 = true;
			cout << "Pair made " << pCard2 << " with " << j << "priority " << b << "\n";
		}
	}
 	if(match1 == true && match2 == true)
	return priority;
	else return -1;
}