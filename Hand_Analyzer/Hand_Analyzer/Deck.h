#pragma once
#include <string>
#include <map>

using namespace std;
class Deck
{
public:
	Deck(void);
	~Deck(void);
	void setPocketCards(int c1, int c2);
	void addTableCard(int card);
	int getTotalCardsRemaining();
	void resetDeck();
	

private:
	//string is card name, bool is if it is in play
	bool deck[52];
	int pocketCards[2];
	int tableCards[5];
	friend class Odds;
	
};

