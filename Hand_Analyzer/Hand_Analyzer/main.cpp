#include <iostream>
#include <string>
#include <math.h>
#include "Deck.h"
#include "Odds.h"
using namespace std;

int main()
{
	cout << "This program will calculate the odds of the top hands you can make while playing Texas Holdem" << endl;
	//system("PAUSE");
	
	Deck myDeck = Deck();
	myDeck.setPocketCards(10,2);
	myDeck.addTableCard(23);
	myDeck.addTableCard(4);
	myDeck.addTableCard(16);
	cout << "cards remaining = " << myDeck.getTotalCardsRemaining() << "\n";
	cout << "cards in play = " << 52 - myDeck.getTotalCardsRemaining() << "\n";
	Odds x = Odds();
	x.oddsMakeHand(myDeck);
	myDeck.resetDeck();

	cout << "cards remaining = " << myDeck.getTotalCardsRemaining() << "\n";
	cout << "cards in play = " << 52 - myDeck.getTotalCardsRemaining() << "\n";
	system("PAUSE");
	//because you cannot see the cards of your oppenent this calculator will only tell you the odds of
	//making the best hand(s)
	//later on I plan to add analysis based on pot size, blinds, stack size, opponents playing style etc.

	//odds to take into consideration
	//pot odds: ex you need to bet $10 for a chance to win a $50 dollar pot, you have 5 to 1 odds

	//to calculate odds, count the number of outs, calculate the chance of hitting your hand (this is done by
	//dividing the total number of cards unknown in deck by the number of outs in your hand, if these odds are
	//higher than the pot odds, you should continue to statiscally win more than less
}