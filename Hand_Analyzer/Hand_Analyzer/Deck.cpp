#include "Deck.h"


Deck::Deck(void)
{
	//creates cards and sets the second field -1, meaning false (card is not in use)
	for(int i = 0; i < 52; i++)
		{
			deck[i] = false;
		}
	pocketCards[0] = -1;
	pocketCards[1] = -1;
	for (int i = 0; i < 5; i++)
		tableCards[i] = -1;
}


Deck::~Deck(void)
{
//	delete deck;
}

void Deck::setPocketCards(int c1, int c2)
{
	pocketCards[0] = c1;
	pocketCards[1] = c2;
	deck[c1] = true;
	deck[c2] = true;
}

void Deck::addTableCard(int card)
{
	for(int i = 0; i < 5; i++)
	{
		if(tableCards[i] = -1)
		{
		tableCards[i] = card;
		//set it as in use in the deck
		deck[card] = true;
		i = 10;
		}
	}
}

int Deck::getTotalCardsRemaining()
{
	int remaining = 0;
	for(int i = 0; i < 52; i++)
	{
		if(deck[i] == false)
			remaining++;
	}
	return remaining;
}

void Deck::resetDeck()
{
	//creates cards and sets the second field -1, meaning false (card is not in use)
	for(int i = 0; i < 52; i++)
		{
			deck[i] = false;
		}
	pocketCards[0] = -1;
	pocketCards[1] = -1;
	for (int i = 0; i < 5; i++)
		tableCards[i] = -1;

}
