package com.craig.mike.tipcalculator;

import java.text.DateFormat;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class about extends Activity {
	   public void onCreate(Bundle savedInstanceState) {
		   super.onCreate(savedInstanceState);
	       setContentView(R.layout.about);
	       String currentDateTimeString = DateFormat.getDateInstance().format(new Date());
	       
	       TextView dateText = (TextView)findViewById(R.id.date);
	       dateText.setText(currentDateTimeString);
	   }
	   
}

