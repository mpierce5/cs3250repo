package com.craig.mike.tipcalculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;


public class TipCalculatorActivity extends Activity {
    /** Called when the activity is first created. */
    private EditText mealPriceText, taxRateText; 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
        
        Button calcButton = (Button) findViewById(R.id.calculatebutton);
        calcButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View view)
        	{
        		RadioButton fifteenPercentRadio = (RadioButton) findViewById(R.id.fifteenpercentradibutton);
        		RadioButton tenPercentRadio = (RadioButton) findViewById(R.id.tenpercentradiobutton);
        		float tipPerc;
        		mealPriceText = (EditText) findViewById(R.id.editpriceofmeal);
        		taxRateText = (EditText) findViewById(R.id.edittaxrate);
        		if(fifteenPercentRadio.isChecked())
        			tipPerc = .15f;
        		else if(tenPercentRadio.isChecked())
        			tipPerc = .1f;
        		else
        		{
        		Toast.makeText(TipCalculatorActivity.this, "Please select a tip rate", 3000);
        		return;
        		}
        		
        		try
        		{
        			float priceOfMeal = Float.parseFloat(mealPriceText.getText().toString());
        			float taxRate = Float.parseFloat(taxRateText.getText().toString());
        			
        			float totalOwed = priceOfMeal * (taxRate * .01f) + priceOfMeal + priceOfMeal * tipPerc;
        			TextView toChange = (TextView)findViewById(R.id.amountdue);
        			toChange.setText("$" + String.valueOf(totalOwed));
        			// amountDue = (EditText) findViewById(R.id.amountdue);
        			//amountDue.setText();
        			
        		
        			
        		}catch (Exception e)
        		{
        			Toast.makeText(TipCalculatorActivity.this, "Invalid entry", 3000).show();	
        		}
           	}
        });
        	
        Button aboutButton = (Button) findViewById(R.id.aboutbutton);
        aboutButton.setOnClickListener(new OnClickListener() {
        	public void onClick(View view)
        	{
        	   		Intent i = new Intent(TipCalculatorActivity.this, about.class);
            		startActivity(i);
            		// Some feedback to the user
        	}	
        });         
    }    
}