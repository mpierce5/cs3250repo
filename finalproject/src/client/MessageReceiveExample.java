package client;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: ryan
 * Date: 12/1/11
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class MessageReceiveExample {
    public MessageReceiveExample(){
        Client c = null;
        final JList j = new JList();
        c.addMessageReceivedListener(new MessageReceiveListener() {
            public void onReceive(MessageEvent evt) {
                j.add(new JLabel(evt.getName()+":"+evt.getMessage()));
            }
        });
    }
}
