package client.gui;

import client.*;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: 12/1/11
 * Time: 3:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class Gui
{
    public static void main(String args[])
    {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                appGui myGui = new appGui();
                myGui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }
}

class appGui extends JFrame{
    JPanel myPanel, southernPanel;
    JList chatBox, usersLoggedIn;
    JTextField inputTextBox;
    JLabel connectionStatusLabel;
    JButton sendTextButton;
    JPanel northernPanel;
    JMenuBar myMenuBar;
    JMenu menu;
    JMenuItem connectMenu, disconnectMenu;
    DefaultListModel usersList, chatList;
    Client client;

    boolean isConnected = false;

    appGui()
    {
        client = new ClientImpl();
        setVisible(true);
        setSize(500, 600);
        myPanel = new JPanel(new BorderLayout());
        southernPanel = new JPanel(new BorderLayout());
        inputTextBox = new JTextField();
        inputTextBox.setEditable(false);
        connectionStatusLabel = new JLabel("DISCONNECTED");
        sendTextButton= new JButton("Send");
        usersList = new DefaultListModel();
        chatList = new DefaultListModel();
        chatBox = new JList(chatList);
        usersLoggedIn = new JList(usersList);
        sendTextButton.setEnabled(false);



        usersLoggedIn.setFixedCellWidth(100);
        Font displayFont = new Font("Serif", Font.BOLD, 17);
        usersLoggedIn.setFont(displayFont);
        JScrollPane listPane = new JScrollPane(usersLoggedIn);

        Border listPanelBorder = BorderFactory.createTitledBorder("Users Online");
        listPane.setBorder(listPanelBorder);
        listPane.setBackground(Color.WHITE);

        myPanel.add(listPane, BorderLayout.WEST);
        chatBox.setFont(displayFont);

        JScrollPane chatPane = new JScrollPane(chatBox);


        //JPanel chatPanel = new JPanel();
        Border chatPanelBorder = BorderFactory.createTitledBorder("Chat Area");
        chatPane.setBorder(chatPanelBorder);
        chatPane.setBackground(Color.WHITE);
        chatPane.setBorder(chatPanelBorder);
        myPanel.add(chatPane, BorderLayout.CENTER);

        southernPanel.add(inputTextBox, BorderLayout.CENTER);
        southernPanel.add(sendTextButton, BorderLayout.EAST);
        myPanel.add(southernPanel, BorderLayout.SOUTH);
        northernPanel = new JPanel(new BorderLayout());
        northernPanel.add(connectionStatusLabel, BorderLayout.EAST);
        myPanel.add(northernPanel, BorderLayout.NORTH);

        myMenuBar = new JMenuBar();
        setJMenuBar(myMenuBar);
        menu = new JMenu("Menu");
        myMenuBar.add(menu);
        connectMenu = new JMenuItem("Connect");



        disconnectMenu = new JMenuItem("Disconnect");
        disconnectMenu.setForeground(Color.GRAY);
        menu.add(connectMenu);
        menu.add(disconnectMenu);

        add(myPanel, BorderLayout.CENTER);

        //action listeners
        //client.connect();
        connectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(isConnected)
                    return;
                JTextField nameField = new JTextField();
                JTextField hostField = new JTextField();
                JTextField portField = new JTextField();

                final JComponent[] inputs = new JComponent[] {
                        new JLabel("Username"), nameField,
                        new JLabel("Host Name"), hostField,
                        new JLabel("Port"), portField};
                JOptionPane.showMessageDialog(null, inputs, "Connect To Server", JOptionPane.PLAIN_MESSAGE);
                try{
                    isConnected = client.connect(nameField.getText(), hostField.getText(), Integer.parseInt(portField.getText()));
                }catch(Exception f) {

                }
                if(!isConnected)
                {
                    JOptionPane.showMessageDialog(null, "Connection failed");
                    return;
                }
                disconnectMenu.setForeground(Color.BLACK);
                connectMenu.setForeground(Color.GRAY);
                disconnectMenu.setEnabled(true);
                connectMenu.setEnabled(false);
                connectionStatusLabel.setText("CONNECTED");
                inputTextBox.setEditable(true);
                sendTextButton.setEnabled(true);

                for(String s: client.getAllClients())
                {
                    usersList.add(0,s);
                }
            }


        });

        //recieving a message
        client.addMessageReceivedListener(new MessageReceiveListener() {
            public void onReceive(MessageEvent evt) {
                chatList.add(chatList.getSize(), evt.getName() + " : " + evt.getMessage());
                chatBox.ensureIndexIsVisible(chatList.getSize() -1);
            }
        });

        //sending a message
        inputTextBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println(inputTextBox.getText());
                client.sendMessage(inputTextBox.getText());
                inputTextBox.setText("");

            }
        });

        sendTextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.sendMessage(inputTextBox.getText());
                inputTextBox.setText("");
            }
        });

        //display connected clients
        client.addClientConnectionListener(new ClientConnectionListener() {
            public void onClientConnect(ClientConnectionEvent evt) {
                 usersList.add(0, evt.getName());
                chatList.add(chatList.getSize(), evt.getName() + " connected");
                chatBox.ensureIndexIsVisible(chatList.getSize() - 1);
            }

            public void onClientDisconnect(ClientConnectionEvent evt) {
                usersList.remove(usersList.indexOf(evt.getName()));
                chatList.add(chatList.getSize(), evt.getName() + " disconnected");
                chatBox.ensureIndexIsVisible(chatList.getSize() -1);
            }
        });

        disconnectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!isConnected)
                    return;

                usersList.clear();
                disconnectMenu.setForeground(Color.GRAY);
                connectMenu.setForeground(Color.BLACK);
                disconnectMenu.setEnabled(false);
                connectMenu.setEnabled(true);
                isConnected = false;
                connectionStatusLabel.setText("DISCONNECTED");
                inputTextBox.setEditable(false);
                sendTextButton.setEnabled(false);
                client.disconnect();
            }
        });


    }


}
