package client;

/**
 * Created by IntelliJ IDEA.
 * User: ryan
 * Date: 12/1/11
 * Time: 2:37 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ClientConnectionListener {
    public void onClientConnect(ClientConnectionEvent evt);
    public void onClientDisconnect(ClientConnectionEvent evt);
}