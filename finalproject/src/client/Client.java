package client;

//


import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ryan
 * Date: 12/1/11
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Client {

    /**
     * This method connects to the server
     * @param name
     * @param host
     * @param port
     */
    public boolean connect(String name, String host, int port);
    /**
     * Send a message to the server
     * @param message
     */
    public void sendMessage(String message);

    public void disconnect();

    public void addMessageReceivedListener(MessageReceiveListener l);

    public void addClientConnectionListener(ClientConnectionListener l);

    public List<String> getAllClients();
}