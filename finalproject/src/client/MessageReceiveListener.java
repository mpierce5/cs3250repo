package client;

/**
 * Created by IntelliJ IDEA.
 * User: ryan
 * Date: 12/1/11
 * Time: 2:26 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MessageReceiveListener {

    public void onReceive(MessageEvent evt);
}
