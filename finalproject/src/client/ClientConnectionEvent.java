package client;

/**
 * Created by IntelliJ IDEA.
 * User: ryan
 * Date: 12/1/11
 * Time: 2:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClientConnectionEvent {
    private final String name;
    public ClientConnectionEvent(String name){
        this.name=name;
    }

    public String getName(){
        return name;
    }
}
