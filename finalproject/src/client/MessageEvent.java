package client;

/**
 * Created by IntelliJ IDEA.
 * User: ryan
 * Date: 12/1/11
 * Time: 2:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class MessageEvent {

    private final String name;
    private final String message;
    public MessageEvent(String name, String message){
        this.name=name;
        this.message=message;
    }

    public String getName(){
        return name;
    }

    public String getMessage(){
        return message;
    }
}
