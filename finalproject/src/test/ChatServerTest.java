package test;

import client.Client;
import client.ClientConnectionEvent;
import client.ClientConnectionListener;
import client.ClientImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import server.ChatServer;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ryan
 * Date: 12/6/11
 * Time: 11:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChatServerTest {

    ChatServer server;
    @Before
    public void setUp() throws Exception {
        server = new ChatServer(9999);
        server.start();
    }

    @After
    public void tearDown() throws Exception {
        //server.stop();
    }

    @Test
    public void sendMessage(){
        Client c = new ClientImpl();
        c.connect("Ryan","localhost",9999);
        c.sendMessage("This is a triumph!");
        c.disconnect();
    }

    @Test
    public void listClients() throws InterruptedException {
        Client c1 = new ClientImpl();
        c1.connect("Ryan", "localhost", 9999);

        final Client c2 = new ClientImpl();
        c2.connect("Jimbob","localhost",9999);

        Client c3 = new ClientImpl();
        c3.connect("Humpty D.", "localhost", 9999);

        c2.addClientConnectionListener(new ClientConnectionListener() {
            public void onClientConnect(ClientConnectionEvent evt) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void onClientDisconnect(ClientConnectionEvent evt) {
                System.out.println(evt.getName()+" Disconnect");
                List<String> remaining = c2.getAllClients();
                for(String c: remaining){
                    System.out.println(c+" still connected");
                }
            }
        });

        Thread.sleep(1000);
        c1.disconnect();
        Thread.sleep(3000);
        System.out.println("*******************************");
        List<String> clients = c2.getAllClients();
        //List<String> clients = new ArrayList<String>();
        System.out.println("Listing clients");
        for(String c: clients){
            System.out.println(c);
        }

        Thread.sleep(1000);
        c2.disconnect();
    }
}
